﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> koefs = new List<double>() { 1, 1, 4, 0 };
            Polynom first = new Polynom(koefs, 3);


            List<double> koefs2 = new List<double>() { 0, 0, 0, 6,10,22,1,4,3 };
            Polynom second = new Polynom(koefs2, 5);

            Console.WriteLine("First polynom:\n{0}", first.ToString());
            Console.WriteLine("Second polynom:\n{0}", second.ToString());
            Console.WriteLine("\n\nDemo class working\n");

            Console.WriteLine("Adding second to first polynom");
            first.AddPolynoms(second);
            Console.WriteLine("The result of sum: \n{0}",first.ToString());

            Console.WriteLine("\nSubtraction first polynom from second\n");
            Console.WriteLine("First polynom:\n{0}", first.ToString());
            Console.WriteLine("Second polynom:\n{0}", second.ToString());
            second.Substract(first);
            Console.WriteLine("\nThe result: \n{0}", second.ToString());

            Console.WriteLine("\nCalculating polynom x = 2\n");
            Console.WriteLine("Polynom:\n{0}",first.ToString());
            Console.WriteLine("The Result of calculating:\t{0}", first.CalculatePolymon(2).ToString());
            

            Console.ReadKey();

        }
    }
}

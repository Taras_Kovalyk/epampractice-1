﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Polynom
    {
        public int MaxPower { get; private set; }
       
        private List<KeyValuePair<int, double>> elements;
        //initiaization of polynom with list of koefitiens and value of max power
        public Polynom(List<double> koefs, int maxPower)
        {            
            MaxPower = maxPower;
            this.InitPowers(koefs);
            this.Clean();
            this.AddCostants();
        }
        //init powers
        private void InitPowers(List<double> koefs)
        {
            elements = new List<KeyValuePair<int, double>>();
            int temp = MaxPower;
            for (int i = 0; i < koefs.Count; i++)
            {
                elements.Add(new KeyValuePair<int, double>((temp > 0)? temp:0, koefs[i]));
                temp -= 1;
            }

        }
        private Polynom()
        {
            elements = new List<KeyValuePair<int, double>>();
        }

        // cleans polynom from elements with 0 koef
        public void Clean()
        {
            elements.RemoveAll(el => el.Value == 0);
        }
        // the operation of adding two polynoms
        public void AddPolynoms(Polynom outcome)
        {
            Polynom temp = new Polynom();
            temp.elements.AddRange(this.elements);
            temp.elements.AddRange(outcome.elements);

            temp = AddSimularElements(temp);
            
            this.elements = temp.elements;
            this.CountMaxPower();
            this.Sort();
            this.Clean();
        }
        //gets the value of "x" and calculates polynom
        public double CalculatePolymon(double xValue)
        {
            double result = 0;
            for (int i = 0; i < this.elements.Count; i++)
                result += elements[i].Value * Math.Pow(xValue, elements[i].Key);

            return result;
        }
        // the operation of subtraction of two polynoms
        public void Substract(Polynom outcome)
        {
            Polynom temp = new Polynom();
            temp.elements.AddRange(this.elements);
            temp.elements.AddRange(outcome.elements);
            bool flag = true;
            for (int i = 0; i < temp.elements.Count; i++)
            {
                flag = true;
                double element = temp.elements[i].Value;
                for (int j = i; j < temp.elements.Count; j++)
                    if (temp.elements[i].Key == temp.elements[j].Key && i != j)
                    {
                        element -= temp.elements[j].Value;
                        temp.elements.RemoveAt(j);
                        j--;
                        flag = false;
                    }
                temp.elements[i] =(flag)? new KeyValuePair<int, double>(-temp.elements[i].Key, element):
                                          new KeyValuePair<int, double>(temp.elements[i].Key, element);

            }
            this.elements = temp.elements;
            this.CountMaxPower();
            this.Sort();
            this.Clean();
        }
        //multiply two polynoms
        public void Multiply(Polynom outcome)
        {
            Polynom resultPoly = new Polynom();

            var bigPoly = (this.elements.Count > outcome.elements.Count) ? this : outcome;
            var smallPoly = (this.elements.Count < outcome.elements.Count) ? this : outcome;

            for (int i = 0; i < smallPoly.elements.Count; i++)
                for (int j = 0; j < bigPoly.elements.Count; j++)
                    resultPoly.elements.Add(new KeyValuePair<int, double>(bigPoly.elements[j].Key + smallPoly.elements[i].Key,
                                                                          bigPoly.elements[j].Value * smallPoly.elements[i].Value));

            resultPoly = AddSimularElements(resultPoly);
            resultPoly.CountMaxPower();
            resultPoly.Sort();
            resultPoly.Clean();

            this.elements = resultPoly.elements;
            this.MaxPower = resultPoly.MaxPower;
        }
        // add elements in polynom with simular powers
        private Polynom AddSimularElements(Polynom temp)
        {
            for (int i = 0; i < temp.elements.Count; i++)
                for (int j = i; j < temp.elements.Count; j++)
                    if (temp.elements[i].Key == temp.elements[j].Key && i != j)
                    {
                        temp.elements[i] = new KeyValuePair<int, double>(temp.elements[i].Key, temp.elements[i].Value + temp.elements[j].Value);
                        temp.elements.RemoveAt(j);
                        j--;
                    }

            return temp;
        }
        //adds all constants in polynom
        private void AddCostants()
        {
            double sum = elements.Where(r => r.Key == 0).Sum(r => r.Value);
            elements.RemoveAll(el => el.Key == 0);
            elements.Add(new KeyValuePair<int, double>(0, sum));
        }
        //sorts list of polynoms by their power
        private void Sort()
        {
            this.elements.Sort((x, y) => -x.Key.CompareTo(y.Key));
            this.AddCostants();
        }

        private void CountMaxPower()
        {
           this.MaxPower = this.elements.Max(el => el.Key);
        }


        public override string ToString()
        {
            string result = "";

            for (int i = 0; i < elements.Count; i++)
            {
                switch (elements[i].Key)
                {
                    case 1:
                        result += elements[i].Value + "x";
                        break;
                    case 0:
                        result += elements[i].Value;
                        break;
                    default:
                        result += elements[i].Value + "x^" + elements[i].Key;
                        break;
                }
                if (i == elements.Count - 1)
                    continue;
                result += " + ";
            }

            return result;
        }
    }
}

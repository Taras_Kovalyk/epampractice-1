﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    struct Point
    {
        public double X;
        public double Y;

        public Point(double _x, double _y)
        {
            X = _x;
            Y = _y;
        }
    }

    class Rectangle
    {
        public Point PointA { get; private set; }
        public Point PointB { get; private set; } 
        public Point PointC { get; private set; }
        public Point PointD { get; private set; }

        public double Height { get; private set; }
        public double Width { get; private set; }

        //Initializing the rectangle with his diagonal points
        public Rectangle(Point A, Point C)
        {
            if (!AreValidPoints(A, C))
                throw new InvalidOperationException();

            PointA = A;
            PointC = C;

            CountPointsPosition();
        }
        //initializing rectangle with an A point, width and height
        public Rectangle(Point point, double width, double height )
        {
            PointA = point;
            PointB = new Point(point.X, PointA.Y + height);
            PointC = new Point(PointB.X + width, PointB.Y);
            PointD = new Point(PointC.X, PointC.Y);

            Height = height; Width = width;
                
        }
        //initializing rectangle with 4 points
        public Rectangle(Point A,Point B,Point C,Point D )
        {
            if (!AreValidPoints(A, B, C, D))
                throw new InvalidOperationException();
            else
            {
                PointA = A;
                PointB = B;
                PointC = C;
                PointD = D;

                Width = Math.Abs(PointC.X - PointA.X);
                Height = Math.Abs(PointC.Y - PointA.Y);
            }
        }

        //Initializing the rectangle with his diagonal points int values
        public Rectangle(double Ax, double Ay, double Cx, double Cy)
        {
            Point A; A.X = Ax; A.Y = Ay;
            Point C; C.X = Cx; C.Y = Cy;

            if (!AreValidPoints(A, C))
                throw new InvalidOperationException();
            else
            {
                PointA = A;
                PointC = C;
            }

            CountPointsPosition();
        }

        //counting coordinates of points B and D
        private void CountPointsPosition()
        {
            Width = Math.Abs(PointC.X - PointA.X);
            Height = Math.Abs(PointC.Y - PointA.Y);

            PointB = new Point(PointA.X, PointA.Y + Height);

            PointD = new Point(PointC.X, PointC.Y - Height);
            
        }
       
        // checks whether diagonal points are appropriate to build a rectangle
        private bool AreValidPoints(Point A, Point C)
        {
            return ((A.X >= C.X) || (A.Y >= C.Y)) ?  false : true;      
        }
        // checks whether all points are appropriate to build a rectangle 
        private bool AreValidPoints(Point A, Point B, Point C, Point D)
        {
            return ((A.X != B.X) || (A.Y != D.Y) || (C.Y != B.Y) || (C.X != D.X) ) ? false : true;
        }

        // move rectangle on vector (a,b)
        public void MoveRectengle(int a, int b)
        {
            PointA = new Point(PointA.X + a, PointA.Y + b);
            PointB = new Point(PointB.X + a, PointB.Y + b);
            PointC = new Point(PointC.X + a, PointC.Y + b);
            PointD = new Point(PointD.X + a, PointD.Y + b);
        }

        //proportional size changing
        public void ChangeSizeProp(double height, double width)
        {
            PointA = new Point(PointA.X * width, PointA.Y * height);
            PointB = new Point(PointA.X, PointA.Y + height);
            PointC = new Point(PointA.X + width, PointB.Y);
            PointD = new Point(PointC.X, PointA.Y);

            Height = height;
            Width = width;
        }
        // changing size point A remains stable
        public void ChangeSizeStable(double height, double width)
        {
            PointB = new Point(PointA.X, PointA.Y + height);
            PointC = new Point(PointA.X + width, PointB.Y);
            PointD = new Point(PointC.X,PointA.Y);

            Height = height;
            Width = width;
        }
        //return min rectangle that contains both
        public static Rectangle ContainingRect(Rectangle one, Rectangle two)
        {
            double lowestX = (one.PointA.X < two.PointA.X) ? one.PointA.X : two.PointA.X;
            double lowestY = (one.PointA.Y < two.PointA.Y) ? one.PointA.Y : two.PointA.Y;

            double highestX = (one.PointC.X > two.PointC.X) ? one.PointC.X : two.PointC.X;
            double highestY = (one.PointC.Y > two.PointC.Y) ? one.PointC.Y : two.PointC.Y;

            return new Rectangle(lowestX, lowestY, highestX, highestY);

        }
        //returns an intersection of two rectangles or throws an exception if they doesn't intersect
        public static Rectangle Intersect (Rectangle a, Rectangle b)
        {
            Point A; Point C;
            C.X = Math.Min(a.PointC.X, b.PointC.X);
            C.Y = Math.Min(a.PointC.Y, b.PointC.Y);

            A.X = Math.Max(a.PointA.X, b.PointA.X);
            A.Y = Math.Max(a.PointA.Y, b.PointA.Y); 

            if (C.X < A.X && A.Y > C.Y)
                throw new InvalidOperationException();
            else
                return new Rectangle(A, C);
        }
        public override string ToString()
        {
            return"A:(" + PointA.X.ToString() + "," + PointA.Y.ToString() + ")"+
                "\n" + " B:(" + PointB.X.ToString() + "," + PointB.Y.ToString() + ")" +
                "\n" + " C:(" + PointC.X.ToString() + "," + PointC.Y.ToString() + ")" +
                "\n" + " D:(" + PointD.X.ToString() + "," + PointD.Y.ToString() + ")";

            ;
        }

    }
}
